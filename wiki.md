# Lungs Segmentation

This is the planning information of the Lung Segmentation project. This Wiki is intended to collect all the meetings information and general insights regarding this project.

## Goals:
* Provide a deep-learning based application for segmenting lungs in CT-scan images.
* Use Python as the programming language.
* Define a reliable CNN architecture
* Provide a User Interface for interacting with the application

### Leader : ________________________

## Proposed Top-down approach

```mermaid
graph LR;
Dataset-->B(Architecture Definition)-->C(Training)-->D(Validation)-->F(Measuring)-->Visualisation;
F --> E(Tune-up) --> C;
```
    
## Important Dates

* Project start: :white_check_mark: 13-12-2019
* Planning meeting: :clock1: Friday, 13-12-2019 - 11:30
* Sprints: 3
* Sprint size: 1 Week - _ Working days - _ working hours
* Project ends: :warning: 03-01-2020
* Deadline: :red_circle:  06-01-2020

## Sprint goals

* Sprint 1: Standardize the environment creation process. File reading stage.
* Sprint 2: _______________________
* Sprint 3: Develop a presentation layer, with a GUI.

## Requirements for environment

Environment creation commands (Python3 with conda required)

**WINDOWS:**
```bash
pip install virtualenv
python -m venv --system-site-packages venv
conda deactivate
venv\Scripts\activate.bat
```

After creating the environment, install the packages using:
```bash
pip install -r requirements.txt
```

## Coding Standards

In order to improve the code readability, please follow the [Google Coding Standards for Python](http://google.github.io/styleguide/pyguide.html). This guide is explained with useful examples on how to write good Python code.
